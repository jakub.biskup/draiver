﻿using UnityEngine;

namespace DrAIver.Helpers
{
    /// <summary> Generic singleton pattern class. </summary>
    abstract public class Singleton<T> : MonoBehaviour where T : MonoBehaviour
    {
        /// <value> Singleton instance. </value>
        public static T Instance { get; protected set; }

        private void Awake()
        {
            if (Instance != null)
            {
                Destroy(gameObject);
                return;
            }

            Instance = gameObject.GetComponent<T>();

            Awekened();
        }

        protected virtual void Awekened() { }
    }
}
