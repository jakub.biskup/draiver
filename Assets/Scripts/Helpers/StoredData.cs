﻿using UnityEngine;

namespace DrAIver.Helpers
{
    public static class StoredData
    {
        public static int TotalGenerationsCreatedCount
        {
            get { return PlayerPrefs.GetInt("KEY_TOTAL_GENERATIONS_CREATED_COUNT", 0); }
            set { PlayerPrefs.SetInt("KEY_TOTAL_GENERATIONS_CREATED_COUNT", value); }
        }

        public static int TotalChildsCreatedCount
        {
            get { return PlayerPrefs.GetInt("KEY_TOTAL_CHILDS_CREATED_COUNT", 0); }
            set { PlayerPrefs.SetInt("KEY_TOTAL_CHILDS_CREATED_COUNT", value); }
        }
    }
}