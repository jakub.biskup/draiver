﻿using System.Collections.Generic;
using UnityEngine;

namespace DrAIver.Helpers
{
    /// <summary> DrAIver extensions helper class. </summary>
    public static class Extensions
    {
        /// <summary> Calculates the linear vector that produces the interpolant vector value within the range [a,b]. </summary>
        /// <param name="a"> First vector. </param>
        /// <param name="b"> Second vector. </param>
        /// <param name="value">  </param>
        /// <returns> Interpolant vector value within the range [a,b]. </returns>
        public static float InverseLerp(this Vector3 a, Vector3 b, Vector3 value)
        {
            Vector3 AB = b - a;
            Vector3 AV = value - a;
            return Vector3.Dot(AV, AB) / Vector3.Dot(AB, AB);
        }

        /// <summary> Returns the second element of a sequence, or a default value if the sequence contains less than one item. </summary>
        /// <typeparam name="T"> The type of the elements of list. </typeparam>
        /// <param name="list"> The IEnumerable<T> to return the second element of. </param>
        /// <returns> Default(T) if list contains less than 1 item; otherwise, the second element in list. </returns>
        public static T SecondOrDefault<T>(this List<T> list)
        {
            if(list[1] != null)
            {
                return list[1];
            }
            else
            {
                return default(T);
            }
        }

        /// <summary> Adds the elements of the specified collections to the end of the List<T>. </summary>
        /// <typeparam name="T"> The type of the elements of list. </typeparam>
        /// <param name="list"> The IEnumerable<T> to return after adding first and second range. </param>
        /// <param name="firstRange"> The first IEnumerable<T> to add to list. </param>
        /// <param name="secondRange"> The second IEnumerable<T> to add to list. </param>
        /// <returns> The IEnumerable<T> with first and second ranges added. </returns>
        public static List<T> AddRange<T>(this List<T> list, List<T> firstRange, List<T> secondRange)
        {
            list.Capacity += firstRange.Count + secondRange.Count;
            list.AddRange(firstRange);
            list.AddRange(secondRange);

            return list;
        }
    }
}