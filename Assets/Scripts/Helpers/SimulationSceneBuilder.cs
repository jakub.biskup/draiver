﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace DrAIver.Helpers
{
    /// <summary> Simulation scene builder class. </summary>
    public class SimulationSceneBuilder : Singleton<SimulationSceneBuilder>
    {
        [SerializeField] private List<GameObject> sceneManagers;

        /// <summary> Delegate which is called after finished spawning scene managers task. </summary>
        public static System.Action OnSpawningSceneManagersEnd = delegate { };

        protected override void Awekened() 
            => StartCoroutine(SpawnSceneManagersCor());

        private IEnumerator SpawnSceneManagersCor()
        {
            foreach(var manager in sceneManagers)
            {
                Instantiate(manager, transform);
                yield return null;
            }

            OnSpawningSceneManagersEnd();
        }
    }
}