﻿namespace DrAIver.Helpers
{
    /// <summary> Names class containing constant strings. </summary>
    public static class Names
    {
        /// <summary> Input names class which is containing all constant strings related to input (f.e. car driving). </summary>
        public static class InputNames
        {
            public const string VELOCITY_INPUT = "Vertical";
            public const string STEER_INPUT = "Horizontal";
        }

        /// <summary> Tag names class which is containing all constant strings related to tags (f.e. for detecting collisions). </summary>
        public static class TagNames
        {
            public const string WALL_TAG = "Wall";
            public const string CHECKPOINT_TAG = "Checkpoint";
        }

        /// <summary> Tag names class which is containing all scene names. </summary>
        public static class SceneNames
        {
            public const string MAIN_MENU_SCENE = "MainMenuScene";
            public const string SIMULATION_SCENE = "SimulationScene";
        }
    }
}