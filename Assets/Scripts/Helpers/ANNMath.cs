﻿using System.Runtime.InteropServices;
using DrAIver.Simulation;

namespace DrAIver.Helpers
{
    /// <summary> Artificial Neural Network helper math class. </summary>
    public class ANNMath
    {
        /// <summary> Artificial Neural Network activation function. </summary>
        /// <param name="bias"> Activation function parameter. </param>
        /// <param name="layerIndex"> Optional layer index needed to choose given layer activation function. </param>
        /// <returns> Neuron output. </returns>
        public static float GetActivationFunctionResult(float bias, int layerIndex = 0)
        {
            ActivationFunctionType type = GetActivationFunctionType(layerIndex);
            return GetGivenFunctionResult(type, bias);
        }

        private static ActivationFunctionType GetActivationFunctionType(int layerIndex = 0) 
            => ChooseActivationFunctionType(SimulationManager.Instance.CurrentConfig, layerIndex);

        private static ActivationFunctionType ChooseActivationFunctionType(SimulationConfig simulationConfig, int layerIndex = 0)
        {
            if (simulationConfig.IsGeneralActivationFunction)
            {
                return simulationConfig.GeneralActivationFunctionType;
            }
            else
            {
                return simulationConfig.ANNTopology.LayersTopologies[layerIndex].ActivationFunction;
            }
        }

        private static float GetGivenFunctionResult(ActivationFunctionType type, float bias)
        {
            switch (type)
            {
                default:
                case ActivationFunctionType.Sigmoid:
                    return GetSigmoidFunctionResult(bias);
                case ActivationFunctionType.Sign:
                    return GetSignFunctionResult(bias);
                case ActivationFunctionType.Tanh:
                    return GetTanhFunctionResult(bias);
                case ActivationFunctionType.ArcTan:
                    return GetArctanFunctionResult(bias);
                case ActivationFunctionType.ReLu:
                    return GetReLuFunctionResult(bias);
                case ActivationFunctionType.LeakyReLu:
                    return GetLeakyReLuFunctionResult(bias);
            }
        }

        private static float GetSigmoidFunctionResult(float bias)
        {
            float k = Expf(bias);
            return 2 * (k / (1.0f + k)) - 1;
        }

        private static float GetSignFunctionResult(float bias)
        {
            return bias < 0 ? -1f :
                bias == 0 ? 0 : 1f;
        }

        private static float GetTanhFunctionResult(float bias)
        {
            return Tanh(bias);
        }

        private static float GetArctanFunctionResult(float bias)
        {
            return Arctan(bias);
        }

        private static float GetReLuFunctionResult(float bias)
        {
            return bias < 0 ? 0 :
                bias > 1 ? 1 : bias;
        }

        private static float GetLeakyReLuFunctionResult(float bias)
        {
            if(bias < 0)
            {
                return bias / 100;
            }
            else
            {
                return bias > 1 ? 1 : bias;
            }
        }

        [DllImport("DrAIverLibrary")] private static extern float Expf(float value);

        [DllImport("DrAIverLibrary")] private static extern float Tanh(float value);

        [DllImport("DrAIverLibrary")] private static extern float Arctan(float value);
    }
}
