﻿using DrAIver.Gameplay;
using DrAIver.Helpers;

namespace DrAIver.Simulation
{
    /// <summary> Camera manager class. </summary>
    public class CameraManager : Singleton<CameraManager>
    {
        private CarDriver focusedCar;

        private void FixedUpdate() 
            => TryUpdateCamera();

        private void TryUpdateCamera()
        {
            if(GenerationManager.Instance.IsAnyCarDriving)
            {
                TryUpdateFocusedCar();
            }
        }

        private void TryUpdateFocusedCar()
        {
            CarDriver bestCar = GenerationManager.Instance.CurrentBestCar;

            if (bestCar != null && bestCar != focusedCar)
            {
                SetFocusedCarCameraActive(false);
                focusedCar = bestCar;
                SetFocusedCarCameraActive(true);
            }
        }

        private void SetFocusedCarCameraActive(bool isActive)
        {
            if(focusedCar != null)
            {
                focusedCar.SetCameraActive(isActive);
            }
        }
    }
}