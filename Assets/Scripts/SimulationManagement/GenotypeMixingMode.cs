﻿namespace DrAIver.GA
{
    /// <summary> Genotype mixin mode enumeration type. </summary>
    public enum GenotypeMixingMode
    {
        ArithmeticAvg,
        WeightedAvgWithFixedIncrementStep,
        CustomWeightedAvg
    }
}