﻿using DrAIver.Helpers;

namespace DrAIver.Simulation
{
    /// <summary> Simulation manager class. </summary>
    [System.Serializable]
    public class SimulationManager : Singleton<SimulationManager>
    {
        /// <value> Current simulation configuration. </value>
        public SimulationConfig CurrentConfig { get { return currentConfig; } }
        [UnityEngine.SerializeField] private SimulationConfig currentConfig;

        /// <summary> Delegate which is called after simulation start. </summary>
        public static System.Action OnSimulationStarted = delegate { };

        protected override void Awekened()
        {
            DontDestroyOnLoad(this);
            RegisterDelegates();
        }

        private void StartSimulation() 
            => OnSimulationStarted();

        private void RegisterDelegates() 
            => SimulationSceneBuilder.OnSpawningSceneManagersEnd += StartSimulation;

        private void UnregisterDelegates() 
            => SimulationSceneBuilder.OnSpawningSceneManagersEnd -= StartSimulation;
    }
}
