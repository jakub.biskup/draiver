﻿namespace DrAIver.Simulation
{
    /// <summary> Activation function type enumeration. </summary>
    public enum ActivationFunctionType
    {
        Sigmoid,
        Sign,
        Tanh,
        ArcTan,
        ReLu,
        LeakyReLu
    }
}
