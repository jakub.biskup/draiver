﻿using DrAIver.GA;
using DrAIver.Gameplay;
using DrAIver.ArtificialNeuralNetwork;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DrAIver.Simulation
{
    /// <summary> Generation manager class. </summary>
    public class GenerationManager : Helpers.Singleton<GenerationManager>
    {
        [SerializeField] private GameObject carPrefab;

        private List<CarDriver> currentDrivers = new List<CarDriver>();
        private Population currentPopulation;
        private bool isInitialized = false;

        /// <value> Determines if any car in population is driving. </value>
        public bool IsAnyCarDriving { get; private set; } = false;

        /// <value> Current driving cars count. </value>
        public int DrivingCarsCount { get; private set; } = 0;

        /// <value> Current generation number. </value>
        public int GenerationNumber { get; private set; } = 0;

        /// <value> Car with hightest score. </value>
        public CarDriver CurrentBestCar { get; private set; }

        protected override void Awekened()
            => RegisterDelegates();

        private void OnDestroy()
            => UnregisterDelegates();

        private void FixedUpdate()
            => TryUpdateGeneration();

        private void TryUpdateGeneration()
        {
            if (isInitialized)
            {
                CheckIfAnyCarIsDriving();

                if (IsAnyCarDriving)
                {
                    CheckBestCar();
                }
                else
                {
                    OnGenerationEnd();
                }
            }
        }

        private void CheckIfAnyCarIsDriving()
        {
            DrivingCarsCount = currentDrivers.Where(x => x.IsDriving).ToList().Count;
            IsAnyCarDriving = DrivingCarsCount > 0;
        }

        private void CheckBestCar()
            => CurrentBestCar = currentDrivers
                                            .Where(x => x.IsDriving)?
                                            .Aggregate((firstCar, secondCar)
                                                => firstCar.Score > secondCar.Score ? firstCar : secondCar);

        private void RegisterDelegates()
            => SimulationManager.OnSimulationStarted += OnSimulationStarted;

        private void UnregisterDelegates()
            => SimulationManager.OnSimulationStarted -= OnSimulationStarted;

        private void OnSimulationStarted()
        {
            CreateFirstPopulation();
            SpawnNextPopulationDrivers();

            isInitialized = true;
        }

        private void CreateFirstPopulation()
            => CreatePopulation(GenotypeMixer.GetRandom());

        private void CreatePopulation(ANN baseGenotype)
        {
            GAConfig gaConfig = SimulationManager.Instance.CurrentConfig.GAConfig;
            bool shouldAddBestCarsFromLastGeneration = GenerationNumber > 1 && gaConfig.ShouldAddBestCarsFromLastGeneration;

            if (shouldAddBestCarsFromLastGeneration)
            {
                currentPopulation = new Population(baseGenotype, GetCurrentGenerationBestCarsGenotypes(gaConfig.BestCarsFromLastGenerationToAddCount)); ;
            }
            else
            {
                currentPopulation = new Population(baseGenotype);
            }

            OnPopulationCreated();
        }

        private void OnPopulationCreated()
        {
            GenerationNumber++;
            Helpers.StoredData.TotalGenerationsCreatedCount++;
        }

        private void SpawnNextPopulationDrivers()
            => currentPopulation.Drivers.ForEach(SpawnDriver);

        private void SpawnDriver(ANN artificialBrain)
        {
            CarDriver driver = Instantiate(carPrefab, transform).GetComponent<CarDriver>();
            driver.Init(artificialBrain);
            currentDrivers.Add(driver);
            Helpers.StoredData.TotalChildsCreatedCount++;
        }

        private void OnGenerationEnd()
        {
            CreatePopulation(GetCurrentGenerationBaseGenotype());
            DestroyOldPopulation();
            SpawnNextPopulationDrivers();
        }

        private void DestroyOldPopulation()
        {
            currentDrivers.ForEach(x => DestroyImmediate(x.gameObject));
            currentDrivers.Clear();
        }

        private ANN GetCurrentGenerationBaseGenotype()
            => GenotypeMixer.GetMixedGenotype(GetCurrentGenerationBestCarsGenotypes(SimulationManager.Instance.CurrentConfig.GAConfig.CarsConsideredInGenotypeMixing));

        private List<ANN> GetCurrentGenerationBestCarsGenotypes(int bestCarsCount)
        {
            return currentDrivers
                       .OrderByDescending(x => x.Score)
                       .ToList()
                       .GetRange(0, bestCarsCount)
                       .ConvertAll(new System.Converter<CarDriver, ANN>(x => x.Brain.ArtificialBrain));
        }
    }
}