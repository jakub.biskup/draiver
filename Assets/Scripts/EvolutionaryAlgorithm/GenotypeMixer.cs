﻿using DrAIver.ArtificialNeuralNetwork;
using DrAIver.Simulation;
using DrAIver.Helpers;
using UnityEngine;
using System.Collections.Generic;

namespace DrAIver.GA
{
    /// <summary> Genotype mixer class which is helping in evolutionary algorithms for mixing genotypes. </summary>
    public static class GenotypeMixer
    {
        /// <summary> Gets variation from current base. </summary>
        /// <param name="baseGenotype"> Base genotype of ANN. </param>
        /// <returns> Variation of base genotype variable. </returns>
        public static ANN GetVariation(ANN baseGenotype)
        {
            ANN genotypeVariation = new ANN(baseGenotype.Layers);

            for (int i = 0; i < genotypeVariation.Layers.Count; i++)
            {
                genotypeVariation.Layers[i] = GetLayerVariant(baseGenotype.Layers[i]);
            }

            return genotypeVariation;
        }

        /// <summary> Creates random genotype. </summary>
        /// <returns> Random genotype. </returns>
        public static ANN GetRandom()
            => new ANN();

        /// <summary> Creates mixed genotype from given genotypes list. </summary>
        /// <param name="genotypes"> List of genotypes considered in mixing. </param>
        /// <returns> Mixed genotype from given genotypes list. </returns>
        public static ANN GetMixedGenotype(List<ANN> genotypes) 
            => GetBaseGenotype(genotypes);

        private static Layer GetLayerVariant(Layer baseLayer)
        {
            Layer layer = new Layer(baseLayer);

            for (int i = 0; i < baseLayer.Neurons.Count; i++)
            {
                layer.Neurons[i] = GetNeuronVariant(baseLayer.Neurons[i]);
            }

            return layer;
        }

        private static Neuron GetNeuronVariant(Neuron baseNeuron)
        {
            Neuron neuron = new Neuron(baseNeuron);

            for (int i = 0; i < baseNeuron.Weights.Count; i++)
            {
                neuron.Weights[i] = GetNeuronWeight(baseNeuron.Weights[i]);
            }

            return neuron;
        }

        private static float GetNeuronWeight(float baseWeight)
        {
            NeuronsConfig config = SimulationManager.Instance.CurrentConfig.NeuronsConfig;

            if(Random.Range(0f, 1f) < config.WeightsMutationProbability)
            {
                return GetNeuronWeightMutation(config);
            }
            else
            {
               return GetNeuronWeightVariant(baseWeight, config);
            }
        }

        private static float GetNeuronWeightVariant(float baseWeight, NeuronsConfig config)
        {
            float weight = baseWeight;

            if (Random.Range(0f, 1f) < config.WeightsDeviationProbability)
            {
                float deviationRange = config.WeightsRange * config.WeightsDeviationFactor;
                float deviation = Random.Range(-deviationRange, deviationRange);

                weight = Mathf.Clamp(weight + deviation, -config.WeightsRange, config.WeightsRange);
            }

            return weight;
        }

        private static float GetNeuronWeightMutation(NeuronsConfig config) 
            => Random.Range(-config.WeightsRange, config.WeightsRange);

        private static ANN GetBaseGenotype(List<ANN> parentsGenotypes)
        {
            ANN baseGenotype = new ANN();

            for (int layerIndex = 0; layerIndex < baseGenotype.Layers.Count; layerIndex++)
            {
                Layer baseLayer = baseGenotype.Layers[layerIndex];

                for (int neuronIndex = 0; neuronIndex < baseLayer.Neurons.Count; neuronIndex++)
                {
                    Neuron baseNeuron = baseLayer.Neurons[neuronIndex];

                    for (int weightIndex = 0; weightIndex < baseNeuron.Weights.Count; weightIndex++)
                    {
                        baseNeuron.Weights[weightIndex] = ComputeWeight(parentsGenotypes, layerIndex, neuronIndex, weightIndex);
                    }

                    baseLayer.Neurons[neuronIndex] = baseNeuron;
                }

                baseGenotype.Layers[layerIndex] = baseLayer;
            }

            return baseGenotype;
        }

        private static float ComputeWeight(List<ANN> parentsGenotypes, int layerIndex, int neuronIndex, int weightIndex)
        {
            float result = 0;
            int divider = 0;

            for (int i = 0; i < parentsGenotypes.Count; i++)
            {
                int weightMultiplier = GetWeightMultiplier(i);
                float parentWeight = parentsGenotypes[i].Layers[layerIndex].Neurons[neuronIndex].Weights[weightIndex];
                result += parentWeight * weightMultiplier;
                divider += weightMultiplier;
            }

            return result / divider;
        }

        private static int GetWeightMultiplier(int index)
        {
            GAConfig config = SimulationManager.Instance.CurrentConfig.GAConfig;
            GenotypeMixingMode mode = config.GenotypeMixingMode;

            if(mode == GenotypeMixingMode.CustomWeightedAvg && config.CarsConsideredInGenotypeMixing != config.CustomWeightedAvgWeights.Count)
            {
                mode = GenotypeMixingMode.WeightedAvgWithFixedIncrementStep;
                Debug.LogError("GenotypeMixer. GetWeightMultiplier. GA configuration is incompatible with cars count considered in genotype mixing!");
            }

            switch (config.GenotypeMixingMode)
            {
                case GenotypeMixingMode.ArithmeticAvg:
                    return 1;
                case GenotypeMixingMode.WeightedAvgWithFixedIncrementStep:
                    return config.CarsConsideredInGenotypeMixing - index;
                case GenotypeMixingMode.CustomWeightedAvg:
                    return config.CustomWeightedAvgWeights[index];
                default:
                    throw new System.Exception("Unhandled Genotype Mixing mode!");
            }
        }
    }
}