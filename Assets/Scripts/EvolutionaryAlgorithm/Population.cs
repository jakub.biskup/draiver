﻿using DrAIver.Simulation;
using DrAIver.ArtificialNeuralNetwork;
using System.Collections.Generic;

namespace DrAIver.GA
{
    /// <summary> Simulation population class. </summary>
    public class Population
    {
        /// <value> Drivers in given population. </value>
        public List<ANN> Drivers { get; private set; } = new List<ANN>();

        /// <summary> Population constructor. </summary>
        /// <param name="baseGenotype"> Base population genotype. </param>
        /// <param name="unitsToAdd"> Optional car list to add to given population. </param>
        public Population(ANN baseGenotype, List<ANN> unitsToAdd = null)
        {
            CreatePopulation(baseGenotype, unitsToAdd);
        }

        private void CreatePopulation(ANN baseGenotype, List<ANN> unitsToAdd = null)
        {
            if(unitsToAdd != null)
            {
                for(int i = 0; i < unitsToAdd.Count; i++)
                {
                    AddChildToPopulation(unitsToAdd[i]);
                }
            }

            int unitsToSpawn = SimulationManager.Instance.CurrentConfig.PopulationSize - Drivers.Count; ;

            for (int i = 0; i < unitsToSpawn; i++)
            {
                AddChildToPopulation(CreateChild(baseGenotype));
            }
        }

        private ANN CreateChild(ANN baseGenotype) 
            => GenotypeMixer.GetVariation(baseGenotype);

        private void AddChildToPopulation(ANN child)
            => Drivers.Add(child);
    }
}