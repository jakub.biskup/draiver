﻿using System.Collections.Generic;
using UnityEngine;

namespace DrAIver.ArtificialNeuralNetwork
{
    /// <summary> Artificial Neural Network neuron class. </summary>
    [System.Serializable]
    public class Neuron
    {
        /// <value> Neuron weights values getter. </value>
        public List<float> Weights { get; private set; } = new List<float>();

        /// <value> Neuron bias value. </value>
        public float Bias { get; set; } = 0;

        /// <summary> Neuron constructor. </summary>
        /// <param name="inputsCount">Neuron inputs count.</param>
        /// <exception cref="System.Exception">Neuron inputs count value must be greater than 0.</exception>"
        public Neuron(int inputsCount)
        {
            TryCreateNeuron(inputsCount);
        }

        /// <summary> Neuron copy contructor. </summary>
        /// <param name="neuron"> Neuron to copy from. </param>
        public Neuron(Neuron neuron)
        {
            CopyNeuron(neuron);
        }

        private void CopyNeuron(Neuron neuron)
        {
            Weights = new List<float>(neuron.Weights);
            Bias = neuron.Bias;
        }

        private void TryCreateNeuron(int inputsCount)
        {
            if(inputsCount > 0 )
            {
                CreateNeuron(inputsCount);
            }
            else
            {
                throw new System.Exception("Neuron inputs count must be greater than 0!");
            }
        }

        private void CreateNeuron(int inputsCount)
        {
            RandomizeBiasOnCreate();
            RandomizeWeightsOnCreate(inputsCount);
        }

        private void RandomizeBiasOnCreate()
        {
            float biasRange = Simulation.SimulationManager.Instance.CurrentConfig.NeuronsConfig.BiasRange;
            Bias = Random.Range(-biasRange, biasRange);
        }

        private void RandomizeWeightsOnCreate(int inputsCount)
        {
            float weightsRange = Simulation.SimulationManager.Instance.CurrentConfig.NeuronsConfig.WeightsRange;

            for (int i = 0; i < inputsCount; i++)
            {
                Weights.Add(Random.Range(-weightsRange, weightsRange));
            }
        }
    }
}
