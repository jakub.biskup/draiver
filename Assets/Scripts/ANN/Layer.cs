﻿using System.Collections.Generic;

namespace DrAIver.ArtificialNeuralNetwork
{
    /// <summary> Artificial Neural Network layer class. /// </summary>
    [System.Serializable]
    public class Layer
    {
        /// <value> Current layer neurons getter. </value>
        public List<Neuron> Neurons { get; private set; } = new List<Neuron>();

        /// <summary> Layer constructor. </summary>
        /// <param name="neuronsCount">Number of neurons in layer.</param>
        /// <param name="neuronsInputsCount">Number of neuron inputs.</param>
        public Layer(int neuronsCount, int neuronsInputsCount)
        {
            CreateLayer(neuronsCount, neuronsInputsCount);
        }

        /// <summary> Layer copy contructor. </summary>
        /// <param name="layer"> Layer to copy from. </param>
        public Layer(Layer layer)
        {
            CopyLayer(layer);
        }

        /// <summary> Layer outputs getter. </summary>
        /// <returns> Returns layer biases. </returns>
        public List<float> GetLayerOutputs() 
            => GetLayerBiases();

        private void CopyLayer(Layer layer) 
            => Neurons = new List<Neuron>(layer.Neurons);

        private void CreateLayer(int neuronsCount, int neuronsInputsCount)
        {
            for (int i = 0; i < neuronsCount; i++)
            {
                Neurons.Add(new Neuron(neuronsInputsCount));
            }
        }

        private List<float> GetLayerBiases() 
            => Neurons.ConvertAll(new System.Converter<Neuron, float>(x => x.Bias));
    }
}
