﻿using System.Collections.Generic;

namespace DrAIver.ArtificialNeuralNetwork
{
    /// <summary> Artificial Neural Network interface. </summary>
    public interface IANN
    {
        void SetInputs(List<float> inputs);
        void Compute();
        List<float> GetOutputs();
    }
}