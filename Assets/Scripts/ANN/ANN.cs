﻿using System.Collections.Generic;
using System.Linq;

namespace DrAIver.ArtificialNeuralNetwork
{
    /// <summary> Artificial Neural Network class. </summary>
    [System.Serializable]
    public class ANN :  IANN
    {
        /// <value> Current ANN layers getter. </value>
        public List<Layer> Layers { get; private set; } = new List<Layer>();

        /// <summary> Artificial Neural Network constructor. </summary>
        public ANN()
        {
            CreateANN();
        }

        /// <summary> Artificial Neural Network copy constructor. </summary>
        /// <param name="layers"> Layers to copy from. </param>
        public ANN(List<Layer> layers)
        {
            CopyLayers(layers);
        }

        /// <summary> Method which is setting first layer inputs. </summary>
        /// <param name="inputs"> List of inputs. </param>
        public void SetInputs(List<float> inputs)
            => SetFirstLayerBiases(inputs);

        /// <summary> Method which is computing Artificial Neural Network outputs. </summary>
        public void Compute()
            => ComputeANNOutputs();

        /// <summary> Artificial Neural Network ouputs getter. </summary>
        /// <returns> List of Artificial Neural Network outputs. </returns>
        public List<float> GetOutputs()
            => GetLayerOutputs(Layers.Count - 1);

        private void CreateANN()
            => CreateLayers();

        private void CreateLayers()
        {
            Simulation.SimulationConfig simulationConfig = Simulation.SimulationManager.Instance.CurrentConfig;

            CreateFirstLayer(simulationConfig.ANNTopology.LayersTopologies.First().NeuronsCount, simulationConfig);
            CreateNextLayers(simulationConfig);
        }

        private void CreateFirstLayer(int firstLayerInputsCount, Simulation.SimulationConfig simulationConfig) 
            => CreateLayer(simulationConfig.ANNTopology.LayersTopologies.First().NeuronsCount, firstLayerInputsCount);

        private void CreateLayer(int neuronsCount, int inputsCount)
            => Layers.Add(new Layer(neuronsCount, inputsCount));

        private void CreateNextLayers(Simulation.SimulationConfig simulationConfig)
        {
            for (int i = 1; i < simulationConfig.ANNTopology.LayersTopologies.Count; i++)
            {
                int neuronsCount = simulationConfig.ANNTopology.LayersTopologies[i].NeuronsCount;
                int inputsCount = simulationConfig.ANNTopology.LayersTopologies[i - 1].NeuronsCount;
                CreateLayer(neuronsCount, inputsCount);
            }
        }

        private void SetFirstLayerBiases(List<float> inputs)
        {
            Layer firstLayer = Layers.First();

            for (int i = 0; i < inputs.Count; i++)
            {
                firstLayer.Neurons[i].Bias = inputs[i];
            }
        }

        private void CopyLayers(List<Layer> layers) 
            => Layers = new List<Layer>(layers);

        private void ComputeANNOutputs()
        {
            for (int layerIndex = 1; layerIndex < Layers.Count; layerIndex++)
            {
                ComputeANNLayerOutputs(layerIndex);
            }
        }

        private void ComputeANNLayerOutputs(int layerIndex)
        {
            List<float> previousLayerInputs = GetLayerOutputs(layerIndex - 1);

            for (int neuronIndex = 0; neuronIndex < Layers[layerIndex].Neurons.Count; neuronIndex++)
            {
                ComputeANNLayerNeuronBias(layerIndex, neuronIndex, previousLayerInputs);
            }
        }

        private List<float> GetLayerOutputs(int layerIndex) 
            => Layers[layerIndex].GetLayerOutputs();

        private void ComputeANNLayerNeuronBias(int layerIndex, int neuronIndex, List<float> previousLayerInputs)
        {
            float newBias = 0;

            for (int weightIndex = 0; weightIndex < previousLayerInputs.Count; weightIndex++)
            {
                newBias += Layers[layerIndex].Neurons[neuronIndex].Weights[weightIndex] * previousLayerInputs[weightIndex];
            }

            Layers[layerIndex].Neurons[neuronIndex].Bias = Helpers.ANNMath.GetActivationFunctionResult(newBias, layerIndex);
        }
    }
}
