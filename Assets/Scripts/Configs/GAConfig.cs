﻿using DrAIver.GA;
using System.Collections.Generic;
using UnityEngine;

namespace DrAIver.Simulation
{
    /// <summary> Neuron configuration class. </summary>
    [System.Serializable]
    public class GAConfig
    {
        /// <value> Genotype mixing mode. </value>
        public GenotypeMixingMode GenotypeMixingMode { get { return m_GenotypeMixingMode; } }
        [SerializeField] private GenotypeMixingMode m_GenotypeMixingMode = GenotypeMixingMode.ArithmeticAvg;

        /// <value> Cars count considered in genotype mixing. </value>
        public int CarsConsideredInGenotypeMixing
        { get { return m_CarsConsideredInGenotypeMixing; } set { m_CarsConsideredInGenotypeMixing = value; } }
        [SerializeField] private int m_CarsConsideredInGenotypeMixing = 2;

        /// <value> Determines if best cars from last generation can be added to next generation. </value>
        public bool ShouldAddBestCarsFromLastGeneration { get { return m_ShouldAddBestCarsFromLastGeneration; } }
        [SerializeField] private bool m_ShouldAddBestCarsFromLastGeneration = false;

        /// <value> Determines how many best cars from last generation should be added. </value>
        public int BestCarsFromLastGenerationToAddCount { get { return m_BestCarsFromLastGenerationToAddCount; } }
        [SerializeField] private int m_BestCarsFromLastGenerationToAddCount = 1; 

        /// <value> Weighted average genotype mixing weights. </value>
        public List<int> CustomWeightedAvgWeights { get { return m_CustomWeightedAvgWeights; } }
        [SerializeField] private List<int> m_CustomWeightedAvgWeights = new List<int>(); 
    }
}
