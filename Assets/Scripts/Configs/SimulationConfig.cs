﻿using DrAIver.ArtificialNeuralNetwork;
using UnityEngine;

namespace DrAIver.Simulation
{
    /// <summary> Simulation config class. </summary>
    [System.Serializable]
    public class SimulationConfig
    {
        /// <value> Determines if general function will be general. </value>
        public bool IsGeneralActivationFunction
        { get { return m_IsGeneralActivationFunction; } set { m_IsGeneralActivationFunction = value; } }
        [SerializeField] private bool m_IsGeneralActivationFunction = false;

        /// <value> Artificial Neural Network general activation function type. </value>
        public ActivationFunctionType GeneralActivationFunctionType
        { get { return m_GeneralActivationFunctionType; } set { m_GeneralActivationFunctionType = value; } }
        [SerializeField] private ActivationFunctionType m_GeneralActivationFunctionType = ActivationFunctionType.Sigmoid;

        /// <value> Determines if car should read user input. </value>
        public bool UseUserInput { get { return m_UseUserInput; } }
        [SerializeField] private bool m_UseUserInput = false;

        /// <value> Simulation population size. </value>
        public int PopulationSize
        { get { return m_PopulationSize; } set { m_PopulationSize = value; } }
        [Range(1, 30)] [SerializeField] private int m_PopulationSize = 30;

        /// <value> Maximum time between proper reaching two checkpoints in seconds. </value>
        public float CheckpointsTimeExceededCapInSeconds { get { return m_CheckpointsTimeExceededCapInSeconds; } }
        [SerializeField] private float m_CheckpointsTimeExceededCapInSeconds = 2f; 

        /// <value> Artificial Neural Network topology. </value>
        public ANNTopology ANNTopology { get { return m_ANNTopology; } }
        [SerializeField] private ANNTopology m_ANNTopology;

        /// <value> Neuron configuration. </value>
        public NeuronsConfig NeuronsConfig { get { return m_NeuronsConfig; } }
        [SerializeField] private NeuronsConfig m_NeuronsConfig;

        /// <value> Car configuration. </value>
        public CarConfig CarConfig { get { return m_CarConfig; } }
        [SerializeField] private CarConfig m_CarConfig;

        /// <value> Genetics algorithm configuration. </value>
        public GAConfig GAConfig { get { return m_GAConfig; } }
        [SerializeField] private GAConfig m_GAConfig; 
    }
}