﻿using DrAIver.Simulation;

namespace DrAIver.ArtificialNeuralNetwork
{
    /// <summary> Artificial Neural Network layer topology class. /// </summary>
    [System.Serializable]
    public class LayerTopology
    {
        /// <value> Layer neurons count. </value>
        public int NeuronsCount { get { return m_NeuronsCount; } }
        [UnityEngine.SerializeField] private int m_NeuronsCount = 6;

        /// <value> Layer activation function. </value>
        public ActivationFunctionType ActivationFunction { get { return m_ActivationFunction; } }
        [UnityEngine.SerializeField] private ActivationFunctionType m_ActivationFunction = ActivationFunctionType.Sigmoid;
    }
}
