﻿using UnityEngine;

namespace DrAIver.Simulation
{
    /// <summary> Neuron configuration class. </summary>
    [System.Serializable]
    public class NeuronsConfig
    {
        /// <value> Neuron bias range. </value>
        public float BiasRange { get { return m_BiasRange; } }
        [SerializeField] private float m_BiasRange = 1f;

        /// <value> Neuron weights range. </value>
        public float WeightsRange { get { return m_WeightsRange; } }
        [SerializeField] private float m_WeightsRange = 8f;

        /// <value> Neuron weights deviation factor. </value>
        public float WeightsDeviationFactor
        { get { return m_WeightsDeviationFactor; } set { m_WeightsDeviationFactor = value; } }
        [Range(0f, 0.5f)] [SerializeField] private float m_WeightsDeviationFactor = 0.15f;

        /// <value> Neuron weights deviation probability. </value>
        public float WeightsDeviationProbability { get { return m_WeightsDeviationProbability; } }
        [Range(0f, 1f)] [SerializeField] private float m_WeightsDeviationProbability = 0.5f;

        /// <value> Neuron weights mutation probability. </value>
        public float WeightsMutationProbability
        { get { return m_WeightsMutationProbability; } set { m_WeightsMutationProbability = value; } }
        [Range(0f, 1f)] [SerializeField] private float m_WeightsMutationProbability = 0.01f; 
    }
}
