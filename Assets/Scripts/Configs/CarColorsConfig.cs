﻿using UnityEngine;

namespace DrAIver.Simulation
{
    /// <summary> Car configuration class. </summary>
    [System.Serializable]
    public class CarColorsConfig
    {
        /// <value> Normal driving car color. </value>
        public Color NormalDriving { get { return m_NormalDriving; } }
        [SerializeField] private Color m_NormalDriving = Color.cyan;

        /// <value> Focused car color. </value>
        public Color FocusedDriving { get { return m_FocusedDriving; } }
        [SerializeField] private Color m_FocusedDriving = Color.blue;

        /// <value> Last best car driving color. </value>
        public Color LastBestCarDriving { get { return m_LastBestCarDriving; } }
        [SerializeField] private Color m_LastBestCarDriving = Color.yellow;

        /// <value> Driving car with mutation color. </value>
        public Color MutationDriving { get { return m_MutationDriving; } }
        [SerializeField] private Color m_MutationDriving = Color.red;

        /// <value> Car which finished color. </value>
        public Color Finished { get { return m_Finished; } }
        [SerializeField] private Color m_Finished = Color.green; 

        /// <value> Car which is not driving color. </value>
        public Color NotDriving { get { return m_NotDriving; } }
        [SerializeField] private Color m_NotDriving = Color.gray;
    }
}
