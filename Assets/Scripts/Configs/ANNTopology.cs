﻿using System.Collections.Generic;

namespace DrAIver.ArtificialNeuralNetwork
{
    /// <summary> Artificial Neural Network topology class. /// </summary>
    [System.Serializable]
    public class ANNTopology
    {
        /// <value> Layers topologies. </value>
        public List<LayerTopology> LayersTopologies { get { return m_LayersTopologies; } }
        [UnityEngine.SerializeField] private List<LayerTopology> m_LayersTopologies = new List<LayerTopology>();
    }
}
