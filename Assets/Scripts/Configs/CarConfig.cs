﻿using UnityEngine;

namespace DrAIver.Simulation
{
    /// <summary> Car configuration class. </summary>
    [System.Serializable]
    public class CarConfig
    {
        /// <value> Sensor max distance value. </value>
        public float SensorMaxDistance { get { return m_SensorMaxDistance; } }
        [SerializeField] private float m_SensorMaxDistance = 10f;

        /// <value> Determines if car sensor ray should be visible. </value>
        public bool IsSensorRayVisible { get { return m_IsSensorRayVisible; } }
        [SerializeField] private bool m_IsSensorRayVisible = true;

        /// <value> Car velocity maximum speed. </value>
        public float VelocityMaxSpeed { get { return m_VelocityMaxSpeed; } }
        [SerializeField] private float m_VelocityMaxSpeed = 50f;

        /// <value> Car velocity change speed. </value>
        public float VelocityChangeSpeed { get { return m_VelocityChangeSpeed; } }
        [SerializeField] private float m_VelocityChangeSpeed = 7f;

        /// <value> Car steer change speed. </value>
        public float SteerChangeSpeed { get { return m_SteerChangeSpeed; } }
        [SerializeField] private float m_SteerChangeSpeed = 5f;

        /// <value> Car friction. </value>
        public float Friction { get { return m_Friction; } }
        [SerializeField] private float m_Friction = 6f;

        /// <value> Car colors configuration. </value>
        public CarColorsConfig ColorsConfig { get { return m_ColorsConfig; } }
        [SerializeField] private CarColorsConfig m_ColorsConfig; 
    }
}
