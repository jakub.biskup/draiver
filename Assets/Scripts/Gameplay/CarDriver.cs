﻿using DrAIver.Helpers;
using DrAIver.ArtificialNeuralNetwork;
using System.Collections.Generic;
using UnityEngine;

namespace DrAIver.Gameplay
{
    /// <summary> Representation of car driver controlled by Artificial Neural Network or user. </summary>
    [RequireComponent(typeof(CarDrivingController))]
    [RequireComponent(typeof(CarProgressController))]
    [RequireComponent(typeof(CarSensorsController))]
    [RequireComponent(typeof(CarBodyController))]
    public class CarDriver : MonoBehaviour
    {
        [SerializeField] private CarDrivingController drivingController;
        [SerializeField] private CarProgressController progressController;
        [SerializeField] private CarSensorsController sensorsController;
        [SerializeField] private CarBodyController bodyController;
        [SerializeField] private GameObject cameraHolder;

        private bool isInitialized = false;

        private bool ShouldMakeUpdate { get { return isInitialized && progressController.IsDriving; } }

        /// <value> Driver's brain containing Artificial Neural Network. </value>
        public Brain Brain { get; private set; }

        ///<value> Current car score. </value>
        public int Score { get { return progressController.Score; } }
        
        /// <value> Determines if driver is driving or not. </value>
        public bool IsDriving { get { return progressController.IsDriving; } }

        ///<value> Current car normalized speed. </value>
        public float CarNormalizedSpeed { get { return drivingController.NormalizedSpeed; } }

        ///<value> Current car normalized steer. </value>
        public float CarNormalizedSteer { get { return drivingController.NormalizedSteer; } }

        /// <summary> Car initialization. </summary>
        public void Init(ANN artificialBrain)
            => InitDriver(artificialBrain);

        /// <summary> Activates or deactivates camera. </summary>
        /// <param name="isActive"> Determines if camera should be active or not. </param>
        public void SetCameraActive(bool isActive) 
            => cameraHolder.SetActive(isActive);

        private void InitDriver(ANN artificialBrain)
        {
            InitBrain(artificialBrain);
            InitControllers();

            isInitialized = true;
        }

        private void InitControllers()
        {
            sensorsController.Init();
            drivingController.Init(Brain, bodyController.Body);
            //progressController.Init();
            //bodyController.Init();
        }

        private void InitBrain(ANN artificialBrain)
            => Brain = new Brain(artificialBrain);

        private void FixedUpdate()
            => TryUpdateCar();

        private void TryUpdateCar()
        {
            if(ShouldMakeUpdate)
            {
                if(progressController.IsTimeExceeded())
                {
                    OnDeath();
                }

                UpdateBrain();
                UpdateCarControllers();
            }
        }

        private void UpdateCarControllers()
        {
            sensorsController.UpdateController();
            drivingController.UpdateController();
        }

        private void UpdateBrain()
        {
            Brain.SetANNInputs(GetANNInputs());
            Brain.ComputeANNOutputs();
        }

        private List<float> GetANNInputs()
            => new List<float>().AddRange(sensorsController.GetSensorsMeasuredDistance(), drivingController.GetCurrentCarOutputs());

        private void OnTriggerEnter(Collider collider)
        {
            if (collider.tag == Names.TagNames.CHECKPOINT_TAG)
            {
                OnCheckpointReached(GetCheckpointFromCollision(collider));
            }
        }

        private Checkpoint GetCheckpointFromCollision(Collider collider)
            => collider.gameObject.GetComponent<Checkpoint>();

        private void OnCheckpointReached(Checkpoint checkpoint)
        {
            if (checkpoint.Type == CheckpointType.Segment)
            {
                OnNextCheckpointReached(checkpoint);
            }
            else
            {
                OnTrackFinished();
            }
        }

        private void OnNextCheckpointReached(Checkpoint checkpoint)
            => progressController.OnCheckpointReached(checkpoint);

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.collider.tag == Names.TagNames.WALL_TAG)
            {
                OnDeath();
            }
        }

        private void OnTrackFinished()
            => progressController.OnTrackFinished();

        private void OnDeath()
        {
            progressController.OnDeath();
            sensorsController.DeactivateSensors();
        }
    }
}