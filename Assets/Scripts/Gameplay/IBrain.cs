﻿namespace DrAIver.Gameplay
{
    /// <summary> Brain outputs reader interface. </summary>
    public interface IBrainOutputsReader
    {
        float GetVelocityOutput();
        float GetSteerOutput();
    }
}