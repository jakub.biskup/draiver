﻿namespace DrAIver.Gameplay
{
    /// <summary> Checkpoint type enumeration. </summary>
    [System.Serializable]
    public enum CheckpointType
    {
        Segment,
        Finish
    }
}