﻿using System;
using UnityEngine;

namespace DrAIver.Gameplay
{
    /// <summary> Car progress controller class. </summary>
    public class CarProgressController : MonoBehaviour
    {
        private DateTime lastCheckpointReachTime = DateTime.UtcNow;

        /// <value> Car score. </value>
        public int Score { get; private set; } = 0;

        /// <value> Determines if car is alive or not. </value>
        public bool IsDriving { get; private set; } = true;

        /// <summary> Checks if car exceeded the time for reaching next checkpoint. </summary>
        /// <returns> True if car exceeded time for reaching next checkpoints. </returns>
        public bool IsTimeExceeded()
        {
            TimeSpan timeSpan = DateTime.UtcNow - lastCheckpointReachTime;
            return timeSpan.TotalSeconds > Simulation.SimulationManager.Instance.CurrentConfig.CheckpointsTimeExceededCapInSeconds;
        }

        /// <summary> Callback after checkpoint reached. </summary>
        /// <param name="checkpoint"> Reached checkpoint. </param>
        public void OnCheckpointReached(Checkpoint checkpoint)
        {
            if (IsNextCheckpoint(checkpoint))
            {
                OnNextCheckpointReached(checkpoint);
            }
        }

        /// <summary> Callback after driver death. </summary>
        public void OnDeath() 
            => IsDriving = false;

        /// <summary> Callback after driver finished track. </summary>
        public void OnTrackFinished()
            => IsDriving = false;

        private bool IsNextCheckpoint(Checkpoint checkpoint)
            => checkpoint.Index > Score && checkpoint.Index < Score + 4;

        private void OnNextCheckpointReached(Checkpoint checkpoint)
        {
            Score = checkpoint.Index;
            lastCheckpointReachTime = DateTime.UtcNow;
        }
    }
}