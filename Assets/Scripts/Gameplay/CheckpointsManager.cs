﻿using DrAIver.Helpers;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace DrAIver.Gameplay
{
    /// <summary> Checkpoints manager class. </summary>
    public class CheckpointsManager : Singleton<CheckpointsManager>
    {
        [SerializeField] private Transform track;
        private List<Checkpoint> checkpoints = new List<Checkpoint>();

        /// <summary> Getting checkpoint with given index. </summary>
        /// <param name="index"> Checkpoint index. </param>
        /// <returns> Checkpoint with given index. </returns>
        public Checkpoint GetCheckpoint(int index) 
            => checkpoints.FirstOrDefault(x => x.Index == index);

        protected override void Awekened() 
            => RefreshCheckpointsList();

        private void RefreshCheckpointsList()
        {
            int checkpointIndex = 0;

            checkpoints = new List<Checkpoint>();

            for (int i = 0; i < track.childCount; i++)
            {
                Transform elem = track.GetChild(i);

                List<Checkpoint> cps = elem.GetComponentsInChildren<Checkpoint>().ToList();

                foreach (var cp in cps)
                {
                    cp.Index = checkpointIndex;
                    cp.Type = CheckpointType.Segment;
                    checkpoints.Add(cp);
                    checkpointIndex++;
                }
            }

            checkpoints.LastOrDefault().Type = CheckpointType.Finish;
        }
    }
}
