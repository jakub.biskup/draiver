﻿using DrAIver.Simulation;
using System.Collections.Generic;
using System;
using UnityEngine;

namespace DrAIver.Gameplay
{
    /// <summary> Car sensor class which is computing distance between car and obstacle. </summary>
    public class CarSensor : MonoBehaviour
    {
        [SerializeField] private LineRenderer sensorLineRenderer;
        [SerializeField] private Transform lineSender;
        [SerializeField] private Transform lineSenderDirection;
        [SerializeField] private LayerMask layerToSense;

        private Vector3 senderDirection;

        /// <value> Normalized distance from obstacle. </value>
        public float NormalizedDistance { get; private set; }

        /// <summary> Car sensor initialization. </summary>
        public void Init()
            => CreateLineRendererNewMaterialInstance();

        /// <summary> Updates car sensor. </summary>
        public void UpdateSensor() 
            => Sense();

        /// <summary> Sensor deactivation. </summary>
        public void Deactivate()
        {
            sensorLineRenderer.positionCount = 0;
            lineSender.gameObject.SetActive(false);
        }

        private void CreateLineRendererNewMaterialInstance() 
            => sensorLineRenderer.material = new Material(sensorLineRenderer.material);

        private void Sense()
        {
            ComputeDirection();
            Tuple<Vector3, bool> raycastInfo = SendSensorRaycast();
            ComputeDistance(raycastInfo.Item1);
            TryDrawRay(raycastInfo);
        }

        private void ComputeDirection()
        {
            senderDirection = lineSenderDirection.position - lineSender.position;
            senderDirection.Normalize();
        }

        private Tuple<Vector3, bool> SendSensorRaycast()
        {
            RaycastHit hit;
            if (Physics.Raycast(new Ray(lineSender.position, senderDirection), out hit, SimulationManager.Instance.CurrentConfig.CarConfig.SensorMaxDistance, layerToSense))
            {
                return new Tuple<Vector3, bool>(hit.point, true);
            }
            else
            {
                return new Tuple<Vector3, bool>(lineSender.position + senderDirection * SimulationManager.Instance.CurrentConfig.CarConfig.SensorMaxDistance, false);
            }
        }

        private void ComputeDistance(Vector3 endPoint)
            => NormalizedDistance = ComputeNormalizedDistance(endPoint);

        private float ComputeNormalizedDistance(Vector3 endPoint)
            => Vector3.Distance(lineSender.position, endPoint) / SimulationManager.Instance.CurrentConfig.CarConfig.SensorMaxDistance;

        private void TryDrawRay(Tuple<Vector3, bool> raycastInfo)
        {
            if (SimulationManager.Instance.CurrentConfig.CarConfig.IsSensorRayVisible)
            {
                List<Vector3> points = new List<Vector3>()
                {
                    lineSender.position,
                    raycastInfo.Item1
                };

                sensorLineRenderer.SetPositions(points.ToArray());
                SetLineColor(raycastInfo.Item2);
            }
        }

        private void SetLineColor(bool wasHit)
            => sensorLineRenderer.material.color = wasHit ? Color.red : Color.green;
    }
}
