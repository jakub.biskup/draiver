﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace DrAIver.Gameplay
{
    /// <summary> Car sensors controller class. </summary>
    public class CarSensorsController : MonoBehaviour
    {
        [SerializeField] private List<CarSensor> carSensors;

        /// <summary> Car sensors initialization. </summary>
        public void Init()
            => carSensors.ForEach(x => x.Init());

        /// <summary> Updates sensors controller. </summary>
        public void UpdateController() 
            => carSensors.ForEach(x => x.UpdateSensor());

        /// <summary> Getting every sensor measured distance. </summary>
        /// <returns> Sensors measured distances. </returns>
        public List<float> GetSensorsMeasuredDistance() 
            => carSensors.ConvertAll(new Converter<CarSensor, float>(x => x.NormalizedDistance));

        /// <summary> Sensors deactivation. </summary>
        public void DeactivateSensors() 
            => carSensors.ForEach(x => x.Deactivate());
    }
}
