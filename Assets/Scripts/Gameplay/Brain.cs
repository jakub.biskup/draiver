﻿using DrAIver.Helpers;
using DrAIver.ArtificialNeuralNetwork;
using System.Collections.Generic;
using System.Linq;

namespace DrAIver.Gameplay
{
    /// <summary> Car driver brain class. </summary>
    [System.Serializable]
    public class Brain : IBrainOutputsReader
    {
        private IANN artificialBrain;

        public ANN ArtificialBrain { get { return (ANN)artificialBrain; } }

        /// <summary> Brain class constructor. </summary>
        /// <param name="artificialBrain"> Brain artificial neural network. </param>
        public Brain(IANN artificialBrain)
        {
            this.artificialBrain = artificialBrain;
        }

        /// <summary> Current method is returning Artificial Neural Network calculated output values. </summary>
        /// <param name="annInputs"> Artificial Neural Network input values. </param>
        public void SetANNInputs(List<float> annInputs)
            => ArtificialBrain.SetInputs(annInputs);

        /// <summary> Current method is computing Artificial Neural Network outputs. </summary>
        public void ComputeANNOutputs()
            => ArtificialBrain.Compute();

        /// <summary> Car velocity output getter. </summary>
        /// <returns> Artificial Neural Network calculated velocity ouput. </returns>
        public float GetVelocityOutput() 
            => GetANNOutputs().FirstOrDefault();

        /// <summary> Car steer output getter. </summary>
        /// <returns> Artificial Neural Network calculated steer ouput. </returns>
        public float GetSteerOutput() 
            => GetANNOutputs().SecondOrDefault();

        /// <summary> Current method is returning Artificial Neural Network calculated output values. </summary>
        private List<float> GetANNOutputs()
            => ArtificialBrain.GetOutputs();
    }
}
