﻿using UnityEngine;

namespace DrAIver.Gameplay
{
    /// <summary> Track checkpoint representation. </summary>
    public class Checkpoint : MonoBehaviour
    {
        /// <value> Checkpoint type value. </value>
        public CheckpointType Type { get { return m_Type; } set { m_Type = value; } }
        [SerializeField] private CheckpointType m_Type = CheckpointType.Segment;

        /// <value> Checkpoint index value. </value>
        public int Index { get { return m_Index; } set { m_Index = value; } }
        [SerializeField] private int m_Index = 0;
    }
}
