﻿using DrAIver.Helpers;
using DrAIver.Simulation;
using System.Collections.Generic;
using UnityEngine;

namespace DrAIver.Gameplay
{
    /// <summary> Car driving controller class. </summary>
    public class CarDrivingController : MonoBehaviour
    {
        private IBrainOutputsReader brainOutputsReader;
        private Transform carBody;
        private float currentVelocity = 0f;

        /// <value> Car normalized speed. </value>
        public float NormalizedSpeed { get { return currentVelocity / SimulationManager.Instance.CurrentConfig.CarConfig.VelocityMaxSpeed; } }

        /// <value> Car normalized steer. </value>
        public float NormalizedSteer { get; private set; } = 0f;

        /// <summary> Car driving controller initialization. </summary>
        /// <param name="carBody"> Car body transform. </param>
        /// <param name="brainOutputsReader"> Car driver brain reference. </param>
        public void Init(IBrainOutputsReader brainOutputsReader, Transform carBody)
        {
            this.carBody = carBody;
            this.brainOutputsReader = brainOutputsReader;
        }

        /// <summary> Gets current car outputs such as velocity and steer. </summary>
        /// <returns> Current car outputs (normalized velocity and steer). </returns>
        public List<float> GetCurrentCarOutputs() 
            => new List<float>() { NormalizedSpeed, NormalizedSteer };

        /// <summary> Updates car driving controller. </summary>
        public void UpdateController()
            => UpdateCar();

        private void UpdateCar()
        {
            UpdateCarParameters();
            UpdateCarTransform();
        }

        private void UpdateCarParameters()
        {
            UpdateCarVelocity();
            UpdateCarSteer();
        }

        private void UpdateCarVelocity()
            => ApplyVelocity(ClampInput(ReadInput(Names.InputNames.VELOCITY_INPUT)));

        private float ReadInput(string axisName)
        {
            if (SimulationManager.Instance.CurrentConfig.UseUserInput)
            {
                return GetUserInput(axisName);
            }
            else
            {
                return GetANNInput(axisName);
            }
        }

        private float GetUserInput(string axisName)
            => Input.GetAxis(axisName);

        private float GetANNInput(string axisName)
        {
            if(axisName == Names.InputNames.VELOCITY_INPUT)
            {
                return brainOutputsReader.GetVelocityOutput();
            }
            else
            {
                return brainOutputsReader.GetSteerOutput();
            }
        }

        private float ClampInput(float input, float min = -1, float max = 1)
            => Mathf.Clamp(input, min, max);

        private void ApplyVelocity(float velocityInput)
        {
            CarConfig carConfig = SimulationManager.Instance.CurrentConfig.CarConfig;
            float newVelocity = currentVelocity + velocityInput * carConfig.VelocityChangeSpeed * Time.fixedDeltaTime;
            currentVelocity = Mathf.Clamp(newVelocity, -carConfig.VelocityMaxSpeed, carConfig.VelocityMaxSpeed);

            if(velocityInput == 0)
            {
                ApplyFriction(carConfig);
            }
        }

        private void ApplyFriction(CarConfig carConfig)
        {
            if (currentVelocity > 0)
            {
                currentVelocity = Mathf.Clamp(currentVelocity - carConfig.Friction * Time.fixedDeltaTime, 0, carConfig.VelocityMaxSpeed);
            }
            else if (currentVelocity < 0)
            {
                currentVelocity = Mathf.Clamp(currentVelocity + carConfig.Friction * Time.fixedDeltaTime, -carConfig.VelocityMaxSpeed, 0);
            }
        }

        private void UpdateCarSteer() 
            => ApplySteer(ClampInput(ReadInput(Names.InputNames.STEER_INPUT)));

        private void ApplySteer(float steerInput) 
            => NormalizedSteer = float.IsNaN(-steerInput) ? 0 : steerInput;

        private void UpdateCarTransform()
        {
            RotateCar();
            MoveCar();
        }

        private void RotateCar() 
            => carBody.rotation *= Quaternion.AngleAxis((float)-NormalizedSteer * SimulationManager.Instance.CurrentConfig.CarConfig.SteerChangeSpeed * Time.fixedDeltaTime, Vector3.down);

        private void MoveCar() 
            => carBody.position += carBody.rotation * (currentVelocity * Vector3.forward) * Time.fixedDeltaTime;
    }
}