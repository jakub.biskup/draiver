﻿using UnityEngine;

namespace DrAIver.Gameplay
{
    /// <summary> Car body controller class. </summary>
    public class CarBodyController : MonoBehaviour
    {
        /// <value> Car body transform. </value>
        public Transform Body { get { return m_Body; } }
        [SerializeField] private Transform m_Body; 
    }
}