﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace DrAIver.UI
{
    public class UISimulationSceneBackButton : MonoBehaviour
    {
        public void OnBackButton()
        {
            SceneManager.LoadSceneAsync(Helpers.Names.SceneNames.MAIN_MENU_SCENE);
        }
    }
}