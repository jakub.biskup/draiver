﻿using DrAIver.Helpers;
using UnityEngine;

namespace DrAIver.UI
{
    /// <summary> User interface manager class. </summary>
    public class UIManager : Singleton<UIManager>
    {
    }
}