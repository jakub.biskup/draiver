﻿using UnityEngine;

namespace DrAIver.UI
{
    public class UIPauseResumeButtonsController : MonoBehaviour
    {
        [SerializeField] private GameObject pauseButton;
        [SerializeField] private GameObject resumeButton;

        public void OnPauseButton()
        {
            Time.timeScale = 0;
            pauseButton.SetActive(false);
            resumeButton.SetActive(true);
        }

        public void OnResumeButton()
        {
            Time.timeScale = 1;
            pauseButton.SetActive(true);
            resumeButton.SetActive(false);
        }
    }
}