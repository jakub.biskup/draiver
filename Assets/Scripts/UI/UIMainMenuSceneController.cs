﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace DrAIver.UI
{
    public class UIMainMenuSceneController : MonoBehaviour
    {
        [SerializeField] private GameObject loadingHolder;
        [SerializeField] private GameObject quitOverlayHolder;
        [SerializeField] private float loadingDelay = 1f;

        public void OnEnterLearningModeButton()
        {
            StartCoroutine(LoadingCor());
        }

        private IEnumerator LoadingCor()
        {
            loadingHolder.SetActive(true);

            yield return new WaitForSeconds(loadingDelay);

            SceneManager.LoadSceneAsync(Helpers.Names.SceneNames.SIMULATION_SCENE);

            yield return null;

            loadingHolder.SetActive(false);
        }

        public void OnQuitButton()
        {
            StartCoroutine(QuitCor());
        }

        private IEnumerator QuitCor()
        {
            quitOverlayHolder.SetActive(true);
            yield return null;
            Application.Quit();
        }
    }
}