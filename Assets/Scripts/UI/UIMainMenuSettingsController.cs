﻿using System.Collections;
using TMPro;
using UnityEngine;

namespace DrAIver.UI
{
    public class UIMainMenuSettingsController : MonoBehaviour
    {
        [SerializeField] private GameObject holder;
        [SerializeField] private CanvasGroup canvasGroup;
        [SerializeField] private TextMeshProUGUI generationsCreatedText;
        [SerializeField] private TextMeshProUGUI childsCreatedText;
        [SerializeField] private float fadeDuration = 2f;

        public void OnSettingsButton()
        {
            ShowSettings();
        }

        public void OnSettingsBackButton()
        {
            HideSettings();
        }

        private void ShowSettings()
        {
            StartCoroutine(FadeSettingsCor(true));
        }

        private void HideSettings()
        {
            StartCoroutine(FadeSettingsCor(false));
        }

        private IEnumerator FadeSettingsCor(bool isShowing)
        {
            if(isShowing)
            {
                holder.SetActive(true);
            }

            float phase = 0;
            float from = isShowing ? 0 : 1;
            float to = 1 - from;

            while(phase < 1f)
            {
                SetCanvasGroupAlpha(from, to, phase);
                phase += fadeDuration * Time.deltaTime;
                yield return null;
            }

            SetCanvasGroupAlpha(from, to, 1);

            if(!isShowing)
            {
                holder.SetActive(false);
            }
        }

        private void SetCanvasGroupAlpha(float from, float to, float phase)
        {
            canvasGroup.alpha = Mathf.Lerp(from, to, phase);
        }

        private void Awake()
        {
            RefreshTexts();
        }

        private void RefreshTexts()
        {
            generationsCreatedText.text = string.Format("Total generations created:\n{0}", Helpers.StoredData.TotalGenerationsCreatedCount);
            childsCreatedText.text = string.Format("Total childs created:\n{0}", Helpers.StoredData.TotalChildsCreatedCount);
        }
    }
}