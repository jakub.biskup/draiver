﻿using UnityEngine;

namespace DrAIver.UI
{
    public class UIImageRotation : MonoBehaviour
    {
        [SerializeField] private Vector3 rotation;

        private void Update()
        {
            transform.Rotate(rotation);
        }
    }
}