﻿using DrAIver.Simulation;
using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace DrAIver.UI
{
    public class UIMainMenuSimulationParametersController : MonoBehaviour
    {
        [SerializeField] private GameObject holder;
        [SerializeField] private CanvasGroup canvasGroup;
        [SerializeField] private TextMeshProUGUI populationSizeValueText;
        [SerializeField] private TextMeshProUGUI carsConsideredInCrossingValueText;
        [SerializeField] private TextMeshProUGUI weightsDeviationFactorValueText;
        [SerializeField] private TextMeshProUGUI weightsMutationProbabilityValueText;
        [SerializeField] private float fadeDuration = 2f;

        [Header("Fields")]

        [SerializeField] private Toggle globalActivationFunctionToggle;
        [SerializeField] private TMP_Dropdown globalActivationFunctionDropdown;
        [SerializeField] private Slider populationSizeSlider;
        [SerializeField] private Slider carsConsideredInCrossingSlider;
        [SerializeField] private Slider weightsDeviationFactorSlider;
        [SerializeField] private Slider weightsMutationProbabilitySlider;
        [SerializeField] private Toggle shouldAddBestCarsFromLastGenerationToggle;

        public void OnSimulationParametersButton()
        {
            Show();
        }

        public void OnBackButton()
        {
            Hide();
        }

        public void SetIsGeneralActivationFunction()
        {
            SimulationManager.Instance.CurrentConfig.IsGeneralActivationFunction = globalActivationFunctionToggle.isOn;
        }

        public void SetGeneralActivationFunctionType()
        {
            SimulationManager.Instance.CurrentConfig.GeneralActivationFunctionType = (ActivationFunctionType)globalActivationFunctionDropdown.value;
        }

        public void SetPopulationSize()
        {
            SimulationManager.Instance.CurrentConfig.PopulationSize = (int)populationSizeSlider.value;
            populationSizeValueText.text = SimulationManager.Instance.CurrentConfig.PopulationSize.ToString();
        }

        public void SetCarsConsideredInGenotypeMixing()
        {
            SimulationManager.Instance.CurrentConfig.GAConfig.CarsConsideredInGenotypeMixing = (int)carsConsideredInCrossingSlider.value;
            carsConsideredInCrossingValueText.text = SimulationManager.Instance.CurrentConfig.GAConfig.CarsConsideredInGenotypeMixing.ToString();
        }

        public void SetWeightsDeviationFactor()
        {
            SimulationManager.Instance.CurrentConfig.NeuronsConfig.WeightsDeviationFactor = weightsDeviationFactorSlider.value;
            weightsDeviationFactorValueText.text = weightsDeviationFactorSlider.value.ToString("0.##");
        }

        public void SetWeightsMutationProbability()
        {
            SimulationManager.Instance.CurrentConfig.NeuronsConfig.WeightsMutationProbability = weightsMutationProbabilitySlider.value;
            weightsMutationProbabilityValueText.text = weightsMutationProbabilitySlider.value.ToString("0.##");
        }

        public void SetShouldAddBestCarsFromLastGeneration()
        {
            SimulationManager.Instance.CurrentConfig.IsGeneralActivationFunction = shouldAddBestCarsFromLastGenerationToggle.isOn;
        }

        private void OnEnable()
        {
            globalActivationFunctionToggle.isOn = SimulationManager.Instance.CurrentConfig.IsGeneralActivationFunction;
            globalActivationFunctionDropdown.value = (int)SimulationManager.Instance.CurrentConfig.GeneralActivationFunctionType;
            populationSizeSlider.value = SimulationManager.Instance.CurrentConfig.PopulationSize;
            carsConsideredInCrossingSlider.value = SimulationManager.Instance.CurrentConfig.GAConfig.CarsConsideredInGenotypeMixing;
            weightsDeviationFactorSlider.value = SimulationManager.Instance.CurrentConfig.NeuronsConfig.WeightsDeviationFactor;
            weightsMutationProbabilitySlider.value = SimulationManager.Instance.CurrentConfig.NeuronsConfig.WeightsMutationProbability;
            shouldAddBestCarsFromLastGenerationToggle.isOn = SimulationManager.Instance.CurrentConfig.GAConfig.ShouldAddBestCarsFromLastGeneration;

            populationSizeValueText.text = populationSizeSlider.value.ToString();
            carsConsideredInCrossingValueText.text = carsConsideredInCrossingSlider.value.ToString("0.##");
            weightsDeviationFactorValueText.text = weightsDeviationFactorSlider.value.ToString("0.##");
            weightsMutationProbabilityValueText.text = weightsMutationProbabilitySlider.value.ToString("0.##");
        }

        private void Show()
        {
            StartCoroutine(FadeCor(true));
        }

        private void Hide()
        {
            StartCoroutine(FadeCor(false));
        }

        private IEnumerator FadeCor(bool isShowing)
        {
            if (isShowing)
            {
                holder.SetActive(true);
            }

            float phase = 0;
            float from = isShowing ? 0 : 1;
            float to = 1 - from;

            while (phase < 1f)
            {
                SetCanvasGroupAlpha(from, to, phase);
                phase += fadeDuration * Time.deltaTime;
                yield return null;
            }

            SetCanvasGroupAlpha(from, to, 1);

            if (!isShowing)
            {
                holder.SetActive(false);
            }
        }

        private void SetCanvasGroupAlpha(float from, float to, float phase)
        {
            canvasGroup.alpha = Mathf.Lerp(from, to, phase);
        }
    }
}