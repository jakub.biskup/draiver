﻿using TMPro;
using UnityEngine;

namespace DrAIver.UI
{
    public class UISimulationSceneStatisticsController : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI driversLeftText;
        [SerializeField] private TextMeshProUGUI generationNumberText;
        [SerializeField] private TextMeshProUGUI normalizedSpeedText;
        [SerializeField] private TextMeshProUGUI normalizedSteerText;

        private void Update() 
            => TryRefreshTexts();

        private void TryRefreshTexts()
        {
            if(Simulation.GenerationManager.Instance.IsAnyCarDriving)
            {
                driversLeftText.text = string.Format("Drivers left: {0}", Simulation.GenerationManager.Instance.DrivingCarsCount);
                generationNumberText.text = string.Format("Generation number: {0}", Simulation.GenerationManager.Instance.GenerationNumber);
                normalizedSpeedText.text = string.Format("Speed: {0}", Simulation.GenerationManager.Instance.CurrentBestCar.CarNormalizedSpeed);
                normalizedSteerText.text = string.Format("Steer: {0}", Simulation.GenerationManager.Instance.CurrentBestCar.CarNormalizedSteer);
            }
        }
    }
}